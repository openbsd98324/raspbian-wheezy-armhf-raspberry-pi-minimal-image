


http://www.linuxsystems.it/




Raspbian Wheezy armhf Raspberry Pi minimal image
After the Debian Wheezy armel image I made a new one based on Raspbian armhf. This one is compiled with hard float support, so basically floating point operations are MUCH faster because they are done in hardware instead of software emulation :)

Features include:

A minimal Raspbian Wheezy installation (similar to a netinstall)
Hard Float binaries: floating point operations are done in hardware instead of software emulation, that means higher performances
Disabled incremental updates, means apt-get update is much faster
Workaround for a kernel bug which hangs the Raspberry Pi under heavy network/disk loads
3.6.11+ hardfp kernel with latest raspberry pi patches
Latest version of the firmwares
Fits 1GB SD cards
A very tiny 118MB image: even with a 2GB SD there is a lot of free space
ssh starts by default
The clock is automatically updated using ntp
IPv6 support
Just 14MB of ram usage after the boot
Here is the link to download my custom image:

http://files2.linuxsystems.it/raspbian_wheezy_20140726.img.7z – London, UK
Checksum MD5: 1be9af7fcec38c7238229edf1c5cdb3c

Mirrors:
7zip: md5sum(1be9af7fcec38c7238229edf1c5cdb3c) – File size: 144MB
http://mirrors.node1.hadrill.org.uk/darkbasic/raspbian_wheezy_20140726.img.7z – Amsterdam, Netherlands (1)
https://debianer.puppis.uberspace.de/files/RaspberryPi/raspbian_wheezy_hardfp_20140726.img.7z – Frankfurt, Germany

You will have to extract the image with p7zip:

7za x raspbian_wheezy_20130923.img.7z

Then flash it to your SD with dd:

dd bs=1M if=raspbian_wheezy_20130923.img of=/dev/sdX

Finally, if you have an sd larger than 1GB, grow the partition with gparted (first move the swap partition at the end).

The root password is raspberry.

 

You will have to reconfigure your timezone after the first boot:

dpkg-reconfigure tzdata

The keyboard layout:

dpkg-reconfigure console-data

And the localization:

dpkg-reconfigure locales

 

